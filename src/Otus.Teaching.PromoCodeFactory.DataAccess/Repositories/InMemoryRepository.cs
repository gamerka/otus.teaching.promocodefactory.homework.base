﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult((IEnumerable<T>)Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task AddOneAsync(T entity)
        {
            Data.Add(entity);

            return Task.CompletedTask;
        }

        public Task UpdateOneAsync(T entity)
        {
            var index = Data.FindIndex(_ => _.Id == entity.Id);
            Data[index] = entity;

            return Task.CompletedTask;
        }

        public Task DeleteOneAsync(Guid id)
        {
            var index = Data.FindIndex(_ => _.Id == id);
            Data.RemoveAt(index);

            return Task.CompletedTask;
        }
    }
}