﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <param name="employeeRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> AddEmployee(EmployeeRequest employeeRequest)
        {
            if (employeeRequest == null) 
                return BadRequest("Empty model");

            if (await _employeeRepository.GetByIdAsync(employeeRequest.Id) != null) 
                return BadRequest("Already exists");

            var employeeModel = new Employee
            {
                Id = employeeRequest.Id,
                Email = employeeRequest.Email,
                Roles = employeeRequest.Roles.Select(x => new Role
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FirstName = employeeRequest.FirstName,
                LastName = employeeRequest.LastName,
                AppliedPromocodesCount = employeeRequest.AppliedPromocodesCount
            };

            await _employeeRepository.AddOneAsync(employeeModel);

            return Ok();
        }

        /// <summary>
        /// Обновить данные сотрудника
        /// </summary>
        /// <param name="employeeRequest"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult> UpdateEmployee(EmployeeRequest employeeRequest)
        {
            if (employeeRequest == null)
                return BadRequest("Empty model");

            if (await _employeeRepository.GetByIdAsync(employeeRequest.Id) == null)
                return NotFound("Employee does not exist");

            var employeeModel = new Employee
            {
                Id = employeeRequest.Id,
                Email = employeeRequest.Email,
                Roles = employeeRequest.Roles.Select(x => new Role
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FirstName = employeeRequest.FirstName,
                LastName = employeeRequest.LastName,
                AppliedPromocodesCount = employeeRequest.AppliedPromocodesCount
            };

            await _employeeRepository.UpdateOneAsync(employeeModel);

            return Ok();
        }

        /// <summary>
        /// Удалить запись о сотруднике
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult> RemoveEmployee(Guid id)
        {
            if (await _employeeRepository.GetByIdAsync(id) == null)
                return NotFound("Employee does not exist");

            await _employeeRepository.DeleteOneAsync(id);

            return Ok();
        }
    }
}